from django.test import TestCase
from django.shortcuts import reverse


class VersionViewTestCases(TestCase):
    def setUp(self):
        pass
    def test_drp_list_view(self):
        print("Dashboard Version Testing")
        self.view_url = reverse('dashboard:version')

        # Testing With Unauthenticated User
        response = self.client.get(self.view_url, follow=True)
        self.assertEqual(response.status_code, 200, msg="Dashboard Version Testing")